# Divar Open Source Project

Note: **This project is just an example of my work**

Divar application is a great system for buying and selling new and old goods.
This application has various capabilities such as advanced authentication system, creating advertise for goods, searching between goods and categories and etc.
In this repository, we are faced with the main capabilities of the divar application.

---

### Authentication and Authorization
The USERNAME field uses the phone number like the main application.
After giving the phone number, username and password, the user registers in the system but remains inactive.
The user is activated after confirming their phone number.

The JWT module is used for user login.
The user can receive the token using their phone number and password.

*In this project, the user model is specifically designed using the AbstractBaseUser class.*

---

### Location
In the main app, most searches are based on location.
So this project has a location module.
In this module, cities are classified based on their provinces; like the:

**Tehran**
    `Tehran`
    `Ghods`

**Fars**
    `Shiraz`
    `Fasa`

---

### Category
In the main application, each goods has its own category.
You can also search for goods by category.
This module uses the same technique used in the location module; for example:

**Technology**
    `LapTop`
    `Smart Phoen`

**Car**
    `Sedan`
    `Coupe`

---

### Goods
The main purpose of the Divar application is to buy and sell goods.
That is why the goods system is so important.
In the goods model, it was tried as close as possible to the main application.

#### Goods Explorer
Various views have been created to explore the goods; like that:
- [x] Explorer Goods View
- [x] Retrieve Goods View
- [x] Search by Name Goods View
- [x] Search by Category Goods View
- [x] Search by Location Goods View
- [x] Search by Category or Location Goods View

#### Goods facade capabilities
Different facades to create different behaviors of goods; like that:
- [x] Create Goods View
- [x] Update Goods View
- [x] Destroy Goods View

This view works with precise permissions.

---

### Swagger
Implementation API documentation by Swagger.
