from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .forms import UserCreationForm, UserChangeForm
from .models import User


class UserAdmin(DjangoUserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm
    model = User
    list_display = (
        'id',
        'phone',
        'username',
        'is_active',
        'is_staff',
        'is_superuser',
        'date_joined',
    )
    list_filter = (
        'is_staff',
        'is_active',
        'is_superuser',
    )
    fieldsets = (
        ('Information', {'fields': ('username', 'email', 'password',)}),
        ('Phone Verification', {'fields': ('phone_verified', 'phone_verify_code', 'phone_verify_time', 'phone_send_verify_code_time',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'username', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = (
        'phone',
        'username',
        'email',
    )
    ordering = (
        'pk',
    )


admin.site.register(User, UserAdmin)