import uuid

from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
)
from django.db import models

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )
    phone = models.CharField(
        max_length=13,
        unique=True,
    )
    username = models.CharField(
        max_length=79,
        unique=True,
        blank=True,
        null=True,
    )
    email = models.EmailField(
        max_length=79,
        unique=True,
        blank=True,
        null=True,
    )
    date_joined = models.DateTimeField(
        auto_now_add=True,
    )
    last_login = models.DateTimeField(
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(
        default=True,
    )
    is_staff = models.BooleanField(
        default=False,
    )
    is_superuser = models.BooleanField(
        default=False,
    )

    phone_verified = models.BooleanField(
        default=False,
    )
    phone_verify_code = models.CharField(
        max_length=10,
        null=True,
        blank=True,
    )
    phone_verify_time = models.DateTimeField(
        null=True,
        blank=True,
    )
    phone_send_verify_code_time = models.DateTimeField(
        verbose_name='time of send verify code',
        null=True,
        blank=True,
    )

    USERNAME_FIELD = 'phone'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = [
        'username',
    ]
    objects = UserManager()

    def __str__(self):
        return self.phone

    def get_full_name(self):
        return f'{self.phone}'
