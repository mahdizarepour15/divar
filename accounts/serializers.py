from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.serializers import (
    ModelSerializer,
)

from utilities.accounts.registration import PhoneVerifyCode, CheckPhoneVerifyCode


class UserRegistrationSerializer(ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            'phone',
            'username',
            'password',
        ]
        extra_kwargs = {'username': {'required': True}}

    def create(self, validated_data):
        vd = validated_data
        new_user = get_user_model().objects.create_inactive_user(
            phone=vd['phone'],
            username=vd['username'],
            password=vd['password'],
        )
        phone_verify_code = PhoneVerifyCode(user=new_user)
        phone_verify_code()
        return new_user


class UserActivationSerializer(ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = [
            'phone',
            'phone_verify_code',
        ]

    def update(self, instance, validated_data):
        if validated_data.get('phone_verify_code') is None:
                raise serializers.ValidationError(
                    detail={'phone_verify_code': 'is required!'}
                    )
        vd = validated_data
        check_code = CheckPhoneVerifyCode(vd.get('phone'), vd.get('phone_verify_code'))
        check_code()
        return instance
