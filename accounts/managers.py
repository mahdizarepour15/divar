from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    def _create_user(self, phone, username, password, **kwargs):
        if (phone and username) is None:
            raise ValueError('phone and username must be specified')
        user = self.model(
            phone=phone,
            username=username,
            **kwargs,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, username, password, **kwargs):
        kwargs.setdefault('is_staff', False)
        kwargs.setdefault('is_superuser', False)
        return self._create_user(phone, username, password, **kwargs)

    def create_superuser(self, phone, username, password, **kwargs):
        kwargs.setdefault('is_staff', True)
        kwargs.setdefault('is_superuser', True)
        return self._create_user(phone, username, password, **kwargs)

    def create_inactive_user(self, phone, username, password, **kwargs):
        kwargs.setdefault('is_active', False)
        kwargs.setdefault('is_staff', False)
        kwargs.setdefault('is_superuser', False)
        return self._create_user(phone, username, password, **kwargs)