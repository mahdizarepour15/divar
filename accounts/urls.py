from django.urls import path

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from .views import (
    UserRegistrationView,
    UserActivationView,
)


app_name = 'accounts'
urlpatterns = [
    path(
        'register/',
        UserRegistrationView.as_view(),
        name='registration',
    ),
    path(
        'user-activation/<str:phone>/',
        UserActivationView.as_view(),
        name='user_activation',
    ),
    # Simple JWT
    path(
        'token/',
        TokenObtainPairView.as_view(),
        name='token_obtain_pair',
    ),
    path(
        'token/refresh/',
        TokenRefreshView.as_view(),
        name='token_refresh',
    ),
]