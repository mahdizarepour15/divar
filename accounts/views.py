from django.contrib.auth import get_user_model
from rest_framework.generics import (
    CreateAPIView, 
    UpdateAPIView,
)

from .serializers import (
    UserRegistrationSerializer, 
    UserActivationSerializer,
)


class UserRegistrationView(CreateAPIView):
    queryset = get_user_model()
    serializer_class = UserRegistrationSerializer

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class UserActivationView(UpdateAPIView):
    queryset = get_user_model()
    lookup_field = 'phone'
    serializer_class = UserActivationSerializer

    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)
