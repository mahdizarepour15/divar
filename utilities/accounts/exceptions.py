from rest_framework.exceptions import APIException
from rest_framework import status


class UserNotFoundError(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'User Not Found'
    default_code = 'user error'


class VerifiedError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'User Was Activate'
    default_code = 'verified error'


class CodeExpiredError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Verification Code Was Expired'
    default_code = 'Verification error'


class CodeNotMatchError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = 'Verification Code Did Not Match'
    default_code = 'Verification error'
