from abc import ABC, abstractmethod
from datetime import datetime, timedelta

import numpy as np
from django.contrib.auth import get_user_model

from .exceptions import (
    UserNotFoundError,
    VerifiedError,
    CodeExpiredError,
    CodeNotMatchError,
)


class GenerateBaseCode(ABC):
    @abstractmethod
    def algorithm(self):
        pass
    
    @abstractmethod
    def generate(self):
        pass

    def __call__(self):
        return self.generate()


class GenerateCode(GenerateBaseCode):
    def algorithm(self):
        code = np.random.randint(low=1, high=10, size=(6))
        code_str = ''.join(str(i) for i in code)
        return code_str

    def generate(self):
        return self.algorithm()

    def __call__(self):
        return super().__call__()


class GenerateBaseTime(ABC):
    @abstractmethod
    def algorithm(self):
        pass

    @abstractmethod
    def generate(self):
        return self.algorithm()

    def __call__(self):
        return self.generate()


class GenerateTime(GenerateBaseTime):
    def algorithm(self):
        return datetime.now()

    def generate(self):
        return self.algorithm()

    def __call__(self):
        return super().__call__()


class FetchUser:
    def __init__(self, phone):
        self.phone = phone

    def get_user(self):
        try:
            self.user = get_user_model().objects.get(phone=self.phone)
        except :
            raise UserNotFoundError
        return self.user

    def __call__(self):
        return self.get_user()


class PhoneVerifyCode:
    def __init__(self, user):
        """
        Note: This class work with specific fields name who determined in User model.
        """
        self.user = user

    def save(self):
        self.user.phone_verify_code = GenerateCode().generate()
        self.user.phone_send_verify_code_time = GenerateTime().generate()
        self.user.save()
        return self.user

    def __call__(self):
        return self.save()


class CheckPhoneVerifyCode:
    def __init__(self, phone, code):
        """
            Note: This class work with specific fields name who determined in User model.
        """
        self.user = FetchUser(phone).get_user()
        self.code = code

    def check_verified(self):
        if self.user.phone_verified == False:
            return True
        else:
            raise VerifiedError

    def expire_check(self):
        expire_time = ''.join(str(self.user.phone_send_verify_code_time + timedelta(minutes=2)))
        if expire_time >= str(datetime.now()):
            return True
        else:
            raise CodeExpiredError
    
    def equal_code(self):
        if self.code == self.user.phone_verify_code:
            return True
        else:
            raise CodeNotMatchError

    def activate_user(self):
        if self.check_verified() and self.expire_check() and self.equal_code():
            self.user.is_active = True
            self.user.phone_verified = True
            self.user.phone_verify_time = datetime.now()
            self.user.save()
            return self.user

    def __call__(self):
        return self.activate_user()
