from django.db import models


class Location(models.Model):
    CITY = (
        ('Tehran', (
                ('Tehran', 'Tehran'),
                ('Ghods', 'Ghods'),
            )
        ),
        ('Fars', (
                ('Shiraz', 'Shiraz'),
                ('Fasa', 'Fasa'),
            )
        ),
    )
    id = models.AutoField(
        primary_key=True,
        editable=False,
    )
    city = models.CharField(
        max_length=15,
        choices=CITY,
        default=None,
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    def __str__(self):
        return self.city
