from django.contrib import admin

from .models import Location


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'city',
        'created_at',
    )
    search_fields = (
        'city',
    )
    ordering = (
        '-pk',
    )