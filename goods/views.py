from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView, 
    UpdateAPIView,
    DestroyAPIView,
)
from rest_framework.permissions import IsAuthenticated
from utilities.goods.permissions import IsGoodsOwner
from utilities.accounts.exceptions import APIException

from .models import Goods
from .serializers import (
    ExplorerGoodsSerializer, 
    RetrieveGoodsSerializer,
    CreateGoodsSerializer,
    UpdateGoodsSerializer,
    DestroyGoodsSerializer,
)


class ExplorerGoodsView(ListAPIView):
    serializer_class = ExplorerGoodsSerializer

    def get_queryset(self):
        return Goods.objects.active_items()

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ExplorerGoodsByCategory(ListAPIView):
    serializer_class = ExplorerGoodsSerializer

    def get_queryset(self):
        return Goods.objects.search_in_category(
            category=self.request.query_params.get('category'),
        )

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ExplorerGoodsByNameView(ListAPIView):
    serializer_class = ExplorerGoodsSerializer

    def get_queryset(self):
        return Goods.objects.search_in_names(
            name=self.request.query_params.get('name'),
        )

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ExplorerGoodsByLocation(ListAPIView):
    serializer_class = ExplorerGoodsSerializer

    def get_queryset(self):
        return Goods.objects.search_in_lcoation(
            city=self.request.query_params.get('city'),
        )

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ExplorerGoodsByCategoryOrLocation(ListAPIView):
    serializer_class = ExplorerGoodsSerializer

    def get_queryset(self):
        return Goods.objects.search_in_category_location(
            search=self.request.query_params.get('search'),
        )

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class RetrieveGoodsView(RetrieveAPIView):
    serializer_class = RetrieveGoodsSerializer

    def get_object(self):
        return Goods.objects.get_item(id=self.request.query_params.get('id'))

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class CreateGoodsView(CreateAPIView):
    serializer_class = CreateGoodsSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({'request': self.request})
        return context

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class UpdateGoodsView(UpdateAPIView):
    serializer_class = UpdateGoodsSerializer
    permission_classes = (
        IsAuthenticated,
        IsGoodsOwner
    )

    def get_object(self):
        object = Goods.objects.get_item(id=self.request.query_params.get('id'))
        self.check_object_permissions(self.request, object)
        return object

    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)


class DestroyGoodsView(DestroyAPIView):
    serializer_class = DestroyGoodsSerializer
    permission_classes = (
        IsAuthenticated,
        IsGoodsOwner
    )

    def get_object(self):
        object = Goods.objects.get_item(id=self.request.query_params.get('id'))
        self.check_object_permissions(self.request, object)
        return object

    def perform_destroy(self, instance):
        instance.destroy_goods(instance=instance)

    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)
