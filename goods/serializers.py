from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.serializers import (
    ModelSerializer,
)

from .models import Goods


class ExplorerGoodsSerializer(ModelSerializer):
    class Meta:
        model = Goods
        fields = (
            'id',
            'name',
            'category',
            'location',
            'price',
            'created_at',
        )
        

class RetrieveGoodsSerializer(ModelSerializer):
    user_phone = serializers.CharField(source='user.phone')
    class Meta:
        model = Goods
        fields = (
            'id',
            'user',
            'user_phone',
            'name',
            'category',
            'location',
            'price',
            'description',
            'created_at',
        )


class CreateGoodsSerializer(ModelSerializer):
    class Meta:
        model = Goods
        fields = (
            'name',
            'category',
            'location',
            'price',
            'description',
        )
        extra_kwargs = {
            'price': {'required': True},
            'description': {'required': True},
        }

    def create(self, validated_data):
        new_goods = Goods(**validated_data)
        new_goods.user = self.context.get('request').user
        new_goods.save()
        return new_goods
    

class UpdateGoodsSerializer(ModelSerializer):
    class Meta:
        model = Goods
        fields = (
            'name',
            'category',
            'location',
            'price',
            'description',
        )
    
    def update(self, instance, validated_data):
        instance.name = validated_data.get('name')
        instance.category = validated_data.get('category')
        instance.location = validated_data.get('location')
        instance.price = validated_data.get('price')
        instance.description = validated_data.get('description')
        instance.status = False
        instance.save()
        return instance


class DestroyGoodsSerializer(ModelSerializer):
    class Meta:
        model = Goods
