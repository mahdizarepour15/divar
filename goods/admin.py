from django.contrib import admin

from .models import Goods


@admin.register(Goods)
class GoodsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'name',
        'category',
        'location',
        'status',
        'created_at',
    )
    list_filter = (
        'category',
        'location',
        'status',
    )
    search_fields = (
        'id',
        'name',
    )
    list_editable = (
        'status',
    )
