import uuid

from django.db import models
from django.contrib.auth import get_user_model

from category.models import Category
from location.models import Location

from .managers import GoodsManager


class Goods(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )
    user = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.CASCADE,
        related_name='user_goods',
    )
    name = models.CharField(
        max_length=79,
    )
    category = models.ForeignKey(
        to=Category,
        on_delete=models.CASCADE,
        related_name='category_goods',
    )
    location = models.ForeignKey(
        to=Location,
        on_delete=models.CASCADE,
        related_name='location_goods',
    )
    price = models.PositiveIntegerField(
        default=0,
    )
    description = models.TextField()
    status = models.BooleanField(
        default=False,
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    objects = GoodsManager()

    def __str__(self):
        return self.name

    @classmethod
    def destroy_goods(cls, instance):
        instance.status = False
        instance.save()
        return instance

    class Meta:
        verbose_name = 'goods'
        verbose_name_plural = 'goods'
