from django.urls import path

from .views import (
    ExplorerGoodsView,
    RetrieveGoodsView,
    ExplorerGoodsByNameView,
    ExplorerGoodsByCategory,
    ExplorerGoodsByLocation,
    ExplorerGoodsByCategoryOrLocation,
    CreateGoodsView,
    UpdateGoodsView,
    DestroyGoodsView,
)


app_name = 'goods'
urlpatterns = [
    path(
        '',
        ExplorerGoodsView.as_view(),
        name='explorer',
    ),
    path(
        'retrieve/',
        RetrieveGoodsView.as_view(),
        name='retrieve',
    ),
    path(
        'name/',
        ExplorerGoodsByNameView.as_view(),
        name='explorer_name',
    ),
    path(
        'category/',
        ExplorerGoodsByCategory.as_view(),
        name='explorer_category',
    ),
    path(
        'location/',
        ExplorerGoodsByLocation.as_view(),
        name='explorer_location',
    ),
    path(
        'category-location/',
        ExplorerGoodsByCategoryOrLocation.as_view(),
        name='explorer_category_location',
    ),
    path(
        'create/',
        CreateGoodsView.as_view(),
        name='create',
    ),
    path(
        'update/',
        UpdateGoodsView.as_view(),
        name='update',
    ),
    path(
        'destroy/',
        DestroyGoodsView.as_view(),
        name='destroy',
    ),
]