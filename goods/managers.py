from django.db import models
from django.db.models import Q
from rest_framework.exceptions import NotFound


class GoodsManager(models.Manager):
    def active_items(self):
        return self.filter(status=True)

    def get_item(self, id):
        try:
            return self.get(id=id, status=True)
        except:
            raise NotFound

    def search_in_names(self, name):
        try:
            return self.filter(name__icontains=name, status=True)
        except:
            raise NotFound
    
    def search_in_category(self, category):
        try:
            return self.filter(category__name__icontains=category, status=True)
        except:
            raise NotFound

    def search_in_lcoation(self, city):
        try:
            return self.filter(location__city__icontains=city, status=True)
        except:
            raise NotFound

    def search_in_category_location(self, search):
        try:
            return self.filter(
                Q(category__name__icontains=search) | 
                Q(location__city__icontains=search),
                status=True,
            )
        except:
            raise NotFound
