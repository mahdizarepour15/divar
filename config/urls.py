from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="Divar API",
      default_version='v1',
      description="divar API documentation",
      terms_of_service="",
      contact=openapi.Contact(email="mahdizarepour15@gmail.com"),
      license=openapi.License(name="Divar License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'accounts/',
        include('accounts.urls', namespace='accounts'),
    ),
    path(
        'explorer/',
        include('goods.urls', namespace='goods'),
    ),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
