from django.db import models


class Category(models.Model):
    CATEGORY = (
        ('Technology', (
                ('LapTop', 'LapTop'),
                ('Smart Phoen', 'Smart Phoen'),
            )
        ),
        ('Car', (
                ('Sedan', 'Sedan'),
                ('Coupe', 'Coupe'),
            )
        ),
    )
    id = models.AutoField(
        primary_key=True,
        editable=False,
    )
    name = models.CharField(
        max_length=79,
        choices=CATEGORY,
        default=None,
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    def __str__(self):
        return self.name
